//
//  List.swift
//  ListApp
//
//  Created by Ignacio Chartier on 4/23/17.
//  Copyright © 2017 UCU. All rights reserved.
//

import Foundation

class List: NSObject, NSCoding{

    //List id
    var uuid: String = NSUUID().uuidString
    
    //List name
    var listName:String!
    
    //Array of elements
    var arrayOfElements = [Element]()
    
    var doneElements:Int
    
    //Initialize element's name
    init(listName: String) {
        self.listName = listName
        self.arrayOfElements = [Element]()
        self.doneElements = 0
        
    }
    init(listName: String, arrayOfElements: [Element], doneElements: Int){
        self.listName = listName
        self.arrayOfElements = arrayOfElements
        self.doneElements = doneElements
    }
    
    func addElement(element:Element){
        arrayOfElements.append(element)
    }
    
    func addElementAtIndex(with element:Element, at index:Int){
        arrayOfElements.insert(element, at: index)
    }
    
    func removeElement(at index: Int){
        arrayOfElements.remove(at: index)
    }
    
    func clearList(){
        arrayOfElements.removeAll()
    }
    
    func updateDoneElements(){
        self.doneElements = 0
        for element in arrayOfElements {
            if element.inShoppingList {
                doneElements = doneElements + 1
            }
        }
    }
    
    
    
    // NSCoding
    required convenience init(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "listName") as! String
        var elements = [Element]()
        if let elementsAux = aDecoder.decodeObject(forKey: "elements") as? Data{
            elements = NSKeyedUnarchiver.unarchiveObject(with: elementsAux) as! [Element]
        }
        var doneElements = 0
        if let doneElementsAux = aDecoder.decodeObject(forKey: "doneElements") as? Int{
            doneElements = doneElementsAux
        }
        self.init(listName: name, arrayOfElements: elements, doneElements: doneElements)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(listName, forKey: "listName")
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: arrayOfElements)
        aCoder.encode(encodedData, forKey: "elements")
        aCoder.encode(doneElements, forKey: "doneElements")
        
    }
}
