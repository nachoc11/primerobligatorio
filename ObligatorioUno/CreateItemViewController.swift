//
//  CreateItemViewController.swift
//  ObligatorioUno
//
//  Created by Ignacio Chartier on 5/1/17.
//  Copyright © 2017 UCU. All rights reserved.
//

import UIKit

class CreateItemViewController: UIViewController {

    @IBOutlet weak var elementNameTextField: UITextField!
    @IBOutlet weak var elementQuantityTextField: UITextField!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.tintColor = UIColor.white

    
        saveButton.isEnabled = false
    }
    
    @IBAction func saveElement(_ sender: UIBarButtonItem) {
        if let elementName = elementNameTextField.text{
            var numberElementQuantity = 1
            if let string = elementQuantityTextField.text, let elementQuantity = Int(string){
                if elementQuantity > 0{
                    numberElementQuantity = elementQuantity
                }
            }
            let newElement = Element(elementName: elementName, elementQuantity: numberElementQuantity)
            let result = DataManager.sharedDataManager.saveElement(with: newElement)
            if result {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.navigationController?.popViewController(animated: true)
    }

    
    // Aux Functions
    @IBAction func stepperAction(_ sender: UIStepper) {
        elementQuantityTextField.text = String(Int(stepper.value))
    }
    
    @IBAction func quantityChangedManually(_ sender: UITextField) {
        if let value = Int(elementQuantityTextField.text!) as Int!{
            stepper.value = Double(value)
        }
    }
    @IBAction func elementNameChange(_ sender: UITextField) {
        if let text = sender.text {
            if text.characters.count > 0 && !text.trimmingCharacters(in: .whitespaces).isEmpty{
                saveButton.isEnabled = true
            }else{
                saveButton.isEnabled = false
            }
        }else{
            saveButton.isEnabled = false
        }
    }


}
