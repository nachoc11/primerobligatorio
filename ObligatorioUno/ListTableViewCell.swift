//
//  ListTableViewCell.swift
//  ObligatorioUno
//
//  Created by Ignacio Chartier on 5/1/17.
//  Copyright © 2017 UCU. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    @IBOutlet weak var listNameLabel: UILabel!
    @IBOutlet weak var doneElementsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI(with list: List){
        listNameLabel.text = list.listName
        doneElementsLabel.text = "\(list.doneElements)/\(list.arrayOfElements.count)"
    }

}
