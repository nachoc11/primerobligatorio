//
//  Element.swift
//  ListApp
//
//  Created by Daniel Martinez Condinanza on 4/19/17.
//  Copyright © 2017 UCU. All rights reserved.
//

import Foundation

class Element:NSObject, NSCoding {
    
    //Element id
    var uuid: String = NSUUID().uuidString
    
    //Element name
    var elementName:String
    
    //Element quantity
    var elementQuantity:Int
    
    //Element in shopping list
    var inShoppingList: Bool
    
    init(elementName: String, elementQuantity: Int) {
        self.elementName = elementName
        self.elementQuantity = elementQuantity
        self.inShoppingList = false
        
    }
    
    init(uuid: String, elementName: String, elementQuantity:Int, inShoppingList: Bool) {
        self.uuid = uuid
        self.elementName = elementName
        self.elementQuantity = elementQuantity
        self.inShoppingList = inShoppingList
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let uuid = aDecoder.decodeObject(forKey: "uuid") as! String
        let elementName = aDecoder.decodeObject(forKey: "elementName") as! String
        var inShoppingList = false
        var elementQuantity = 1
        if let elementQ = aDecoder.decodeObject(forKey: "elementQuantity") as? Int{
            elementQuantity = elementQ
        }
        if let inShopList = aDecoder.decodeObject(forKey: "inShoppingList") as? Bool{
            inShoppingList = inShopList
        }
        self.init(uuid: uuid, elementName: elementName, elementQuantity: elementQuantity, inShoppingList: inShoppingList)
    }
    
    //Encode element
    func encode(with aCoder: NSCoder) {
        aCoder.encode(uuid, forKey: "uuid")
        aCoder.encode(elementName, forKey: "elementName")
        aCoder.encode(elementQuantity, forKey: "elementQuantity")
        aCoder.encode(inShoppingList, forKey: "inShoppingList")
    }
    

}
