//
//  DataManager.swift
//  ListApp
//
//  Created by Daniel Martinez Condinanza on 4/25/17.
//  Copyright © 2017 UCU. All rights reserved.
//

import Foundation

class DataManager{

    
    //Singleton
    static let sharedDataManager = DataManager()
    
    //Properties
    var listOfLists: [List]
    var selectedList: List?
    var selectedElement: Element?

    
    //Init
    private init() {
        //TODO: Load
        let defaults = UserDefaults.standard
        if let listDecoded = defaults.object(forKey: "lists") as? Data{
            self.listOfLists = NSKeyedUnarchiver.unarchiveObject(with: listDecoded) as! [List]
        }
        else{
            self.listOfLists = [List]()
        }
    }
    
    //Delete selected list.
    func deleteSelectedList()-> Bool{
        let listOfListsCount = self.listOfLists.count
        self.listOfLists = listOfLists.filter() { $0 !== DataManager.sharedDataManager.selectedList}
        if listOfListsCount == self.listOfLists.count + 1{
            return true
        }
        return false
    }
    
    //Append list to list of lists
    func appendList(with list: List)-> Bool{
        let listOfListsCount = self.listOfLists.count
        self.listOfLists.append(list)
        if listOfListsCount == self.listOfLists.count - 1{
            return true
        }
        return false
    }
    
    //save element in selected list
    func saveElement(with element: Element)->Bool{
        selectedList?.addElement(element: element)
        return true
    }
    
}

