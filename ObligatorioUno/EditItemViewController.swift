//
//  EditItemViewController.swift
//  ObligatorioUno
//
//  Created by Ignacio Chartier on 5/1/17.
//  Copyright © 2017 UCU. All rights reserved.
//

import UIKit

class EditItemViewController: UIViewController {

    @IBOutlet weak var elementNameTextField: UITextField!
    @IBOutlet weak var elementQuantityTextField: UITextField!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let element = DataManager.sharedDataManager.selectedElement{
            elementNameTextField.text = element.elementName
            elementQuantityTextField.text = String(element.elementQuantity)
            if let value = Int(elementQuantityTextField.text!) as Int!{
                stepper.value = Double(value)
            }
        
        }
        doneButton.isEnabled = false

        
    }
    
    @IBAction func saveElement(_ sender: UIBarButtonItem) {
        if let newElementName = elementNameTextField.text as String!{
            if let element = DataManager.sharedDataManager.selectedElement{
                element.elementName = newElementName
            }
        }
        if let newElementQuantity = Int(elementQuantityTextField.text!) as Int!{
            if let element = DataManager.sharedDataManager.selectedElement{
                element.elementQuantity = newElementQuantity
            }
            
        }
        else{
            if let element = DataManager.sharedDataManager.selectedElement{
                element.elementQuantity = 1
            }
            
        }
        
        self.navigationController?.popViewController(animated: true)
    }


    // Aux Functions
    @IBAction func stepperActon(_ sender: UIStepper) {
        elementQuantityTextField.text = String(Int(stepper.value))
    }
    
    @IBAction func quantityChangeManually(_ sender: UITextField) {
        if let value = Int(elementQuantityTextField.text!) as Int!{
            stepper.value = Double(value)
        }
    }
    
    @IBAction func elementNameChange(_ sender: UITextField) {
        if let text = sender.text {
            if text.characters.count > 0 && !text.trimmingCharacters(in: .whitespaces).isEmpty{
                doneButton.isEnabled = true
            }else{
                doneButton.isEnabled = false
            }
        }else{
            doneButton.isEnabled = false
        }
    }
    @IBAction func listNameChange(_ sender: UITextField) {

        
    }

}
