//
//  ListViewController.swift
//  ObligatorioUno
//
//  Created by Ignacio Chartier on 5/1/17.
//  Copyright © 2017 UCU. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var listOfListsTableView: UITableView!
    @IBOutlet weak var createListButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 34.0/255.0, green: 81.0/255.0, blue: 146.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName : UIColor.white
        ]
        self.listOfListsTableView.tableHeaderView = createListButton
        self.listOfListsTableView.tableFooterView = UIView()
  
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.listOfListsTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as? ListTableViewCell{
        UIView.transition(with: listOfListsTableView, duration: 0.75, options: .transitionCrossDissolve, animations: { () -> Void in
            cell.alpha = 1
        }, completion: nil)
        }
    }
    
    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.sharedDataManager.listOfLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as? ListTableViewCell{
            let list = DataManager.sharedDataManager.listOfLists[indexPath.row]
            cell.updateUI(with: list)
            cell.backgroundColor = UIColor(white: 1, alpha: 0.5)
            cell.alpha = 0
            return cell
        }else{
            return UITableViewCell()
        }    }
    
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DataManager.sharedDataManager.selectedList = DataManager.sharedDataManager.listOfLists[indexPath.row]
        
        performSegue(withIdentifier: "viewList", sender: nil)
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete") {action in
            DataManager.sharedDataManager.listOfLists.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .left )
            self.listOfListsTableView.reloadData()
        }
        
        return [deleteAction]
    }

    @IBAction func edit(_ sender: UIBarButtonItem) {
        listOfListsTableView.setEditing(!listOfListsTableView.isEditing, animated: true)
        let newButton = UIBarButtonItem(barButtonSystemItem: (listOfListsTableView.isEditing) ? .done : .edit, target: self, action: #selector(edit(_:)))
        self.navigationItem.setLeftBarButton(newButton, animated: true)
    }



}

