//
//  ItemListViewController.swift
//  ObligatorioUno
//
//  Created by Ignacio Chartier on 5/1/17.
//  Copyright © 2017 UCU. All rights reserved.
//

import UIKit

class ItemListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var navBarTitle: UINavigationItem!
    @IBOutlet weak var emptyListView: UIView!
    @IBOutlet weak var listOfElementsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navBarTitle.title = DataManager.sharedDataManager.selectedList?.listName
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let list = DataManager.sharedDataManager.selectedList{
            if list.arrayOfElements.isEmpty{
                emptyListView.isHidden = false
            }else{
                emptyListView.isHidden = true
            }
        }
        listOfElementsTableView.reloadData()
    }
    

    

    @IBAction func deleteList(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Remove list", message: "Are you sure you want to remove this list?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default){
            UIAlertAction in
            if DataManager.sharedDataManager.deleteSelectedList() {
                self.navigationController?.popViewController(animated: true)
            }
        })
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    

    
    // TableViewDelegate | TableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        self.showEmptyListBackground()
        return 1
    }
    func showEmptyListBackground(){
        if DataManager.sharedDataManager.selectedList?.arrayOfElements.count == 0 {
            self.listOfElementsTableView.backgroundView = emptyListView
            self.listOfElementsTableView.separatorStyle = .none
        }
    }
            
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let list = DataManager.sharedDataManager.selectedList{

            return list.arrayOfElements.count}
        else{
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as? ItemTableViewCell{
            if let list = DataManager.sharedDataManager.selectedList{
                let element = list.arrayOfElements[indexPath.row]
                let accessory: UITableViewCellAccessoryType = element.inShoppingList ? .checkmark : .none
                cell.accessoryType = accessory
                cell.updateUI(with: element)
                return cell
            }
        }
        return UITableViewCell()
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let list = DataManager.sharedDataManager.selectedList{
            let element = list.arrayOfElements[indexPath.row]
            element.inShoppingList = !element.inShoppingList
            list.updateDoneElements()
            
            
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if let list = DataManager.sharedDataManager.selectedList {
            let deleteAction = UITableViewRowAction(style: .default, title: "Delete") {action in
                list.removeElement(at: indexPath.row)
                list.updateDoneElements()
                tableView.deleteRows(at: [indexPath], with: .left)
                self.listOfElementsTableView.reloadData()
            }
            let editAction = UITableViewRowAction(style: .normal, title: "Edit") {action in
                DataManager.sharedDataManager.selectedElement = list.arrayOfElements[indexPath.row]
                self.performSegue(withIdentifier: "EditItem", sender: nil)
            }
            return [deleteAction, editAction]
        }
        return nil
    }


}
