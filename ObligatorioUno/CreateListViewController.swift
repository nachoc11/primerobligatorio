//
//  CreateListViewController.swift
//  ObligatorioUno
//
//  Created by Ignacio Chartier on 5/1/17.
//  Copyright © 2017 UCU. All rights reserved.
//

import UIKit

class CreateListViewController: UIViewController {
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var listNameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        saveButton.isEnabled = false

        
    }


    @IBAction func createList(_ sender: UIButton) {
        if let listName = listNameTextField.text! as String?{
            let list = List(listName: listName, arrayOfElements: [Element](), doneElements: 0)
            if DataManager.sharedDataManager.appendList(with: list){
                self.dismiss(animated: true, completion: nil)
            }
            else{
                let alert = UIAlertController(title: "Error on create", message: "It appears that we couldn't save your list. Please try again", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                
            }
            
        }
    }

    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func listNameChange(_ sender: UITextField) {
        if let text = sender.text {
            if text.characters.count > 0 && !text.trimmingCharacters(in: .whitespaces).isEmpty{
                saveButton.isEnabled = true
            }else{
                saveButton.isEnabled = false
            }
        }else{
            saveButton.isEnabled = false
        }
        
    }

}
